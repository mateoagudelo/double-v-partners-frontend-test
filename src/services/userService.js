export default {

    getAll(username) {
        return fetch(`https://api.github.com/search/users?q=${username}&per_page=10`)
            .then(response => response.json())
            .then((data) => {
                return data
            })
    },

    getByUsername(username) {
        return fetch(`https://api.github.com/users/${username}`)
        .then(response => response.json())
        .then((data) => {
            return data
        })   
    },

    async getUsersWithCountFollowers(users) {
        
        const arrUsers = [];
        const arrFollowers = [];
      
        const fetchPromises = users.map((user) => {
          return fetch(`https://api.github.com/users/${user.login}`)
            .then((response) => response.json())
            .then((obj) => {
              arrUsers.push(user.login);
              arrFollowers.push(obj.followers);
            });
        });
      
        await Promise.all(fetchPromises);
      
        return { arrUsers, arrFollowers };
    }

}