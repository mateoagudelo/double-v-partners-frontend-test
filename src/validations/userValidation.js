export default {

    maxCharacters(username) {
        return username.length >= 4;
    },

    equalToDoublevpartners(username) {
        return username == 'doublevpartners';
    },

    textIsEmpty(username) {
        return username.trim() == "";
    }

}