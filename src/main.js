import { createApp } from 'vue'
import { createPinia } from 'pinia'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { faUser, faSearch, faEye, faUserFriends } from '@fortawesome/free-solid-svg-icons'
import App from './App.vue'
import router from './router'
const app = createApp(App)
library.add(faUser, faSearch, faEye, faUserFriends)
app.use(createPinia())
app.use(router)
app.component('font-awesome-icon', FontAwesomeIcon).mount('#app')