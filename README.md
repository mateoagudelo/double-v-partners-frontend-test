# Double V Partners - Frontend Test

This test was developed in Vue 3 with Composition API. This test is for Double V Partners.

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```

### [DEMO](https://648c66a3218c1621c094f118--papaya-cat-441b08.netlify.app/)
